export class Flight {
    constructor(
        public fullName: string,
        public from: string,
        public to: string,
        public type: string,
        public adults: number,
        public departure: Date,
        public children: number,
        public infants: number,
        public arrival: Date
    ){
        this.fullName = fullName;
        this.from = from;
        this.to = to;
        this.type = type;
        this.adults = adults;
        this.departure = departure;
        this.children = children;
        this.infants = infants;
        this.arrival = arrival;
    }
}
