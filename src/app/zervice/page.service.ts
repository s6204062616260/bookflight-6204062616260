import { Injectable } from '@angular/core';
import { Mockdata } from './mockdata';
import { Flight } from '../components/book-flight/flight';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights: Flight[] = [];
  constructor() { 
    this.flights = Mockdata.tfriend;
  }
  getFlight(): Flight[] {
    return this.flights;
  }
  addFlight(f:Flight): void{
    this.flights.push(f)
  }
}
