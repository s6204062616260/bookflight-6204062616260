import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Flight } from './flight';
import { PageService } from 'src/app/zervice/page.service';
import { AppModule } from 'src/app/app.module';

@Component({
  selector: 'app-book-flight',
  templateUrl: './book-flight.component.html',
  styleUrls: ['./book-flight.component.css']
})
export class BookFlightComponent implements OnInit {
  flights: Array<Flight>=[];
  bookform!: FormGroup;
  constructor(private fb: FormBuilder, private pageService: PageService) { 

  }
  
  onSubmit(f:FormGroup): void{
    if(f.get('FullName')?.value != null && f.get('From')?.value != null && f.get('To')?.value != null && f.get('Type')?.value != null
      && f.get('Adults')?.value != null && f.get('Departure')?.value != null && f.get('Children')?.value != null && f.get('Infants')?.value != null
      && f.get('Arrival')?.value != null){
      let form_record = new Flight(f.get('FullName')?.value,
                                f.get('From')?.value,
                                f.get('To')?.value,
                                f.get('Type')?.value,
                                f.get('Adults')?.value,
                                f.get('Departure')?.value,
                                f.get('Children')?.value,
                                f.get('Infants')?.value,
                                f.get('Arrival')?.value
                                )
      this.pageService.addFlight(form_record);                          
    }  
  }
  ngOnInit(): void {
    this.bookform = this.fb.group({
      FullName: ['',Validators.required],
      From: ['',Validators.required],
      To: ['',Validators.required],
      Type: ['',Validators.required],
      Adults: ['',Validators.required],
      Departure: ['',Validators.required],
      Children: ['',Validators.required],
      Infants: ['',Validators.required],
      Arrival: ['',Validators.required]
    });
    this.getFlightPage();
  }
  getFlightPage(){
    this.flights = this.pageService.getFlight();
  }
  convertDate(f:Date): string{
    return f.toLocaleDateString('th-Th')
  }
}
