import { Flight } from "../components/book-flight/flight";
export class Mockdata {
    public static tfriend: Flight[]= [
        {
            fullName: "Prinyada Juicharoen",
            from: "Don Mueang",
            to: "Phuket",
            type: "One Way",
            adults: 2,
            departure: new Date(2022,2,6),
            children: 1,
            infants: 0,
            arrival: new Date(2022,2,6)
        },
        {
            fullName: "Sittinan Phattananan",
            from: "Don Mueang",
            to: "Phuket",
            type: "Return",
            adults: 2,
            departure: new Date(2022,2,6),
            children: 1,
            infants: 0,
            arrival: new Date(2022,2,15)
        }
    ];
}
